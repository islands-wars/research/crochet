/*
 * Copyright (C) 2018 Islands Wars
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 **/

extern crate iron;
extern crate bodyparser;
extern crate serde;
extern crate serenity;
extern crate md5;
extern crate rand;
extern crate reqwest;
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate serde_json;
#[macro_use(router)] extern crate router;

use iron::prelude::*;
use iron::status;
use iron::mime::Mime;

#[derive(Serialize, Deserialize, Debug)]
struct JsonResponse {
  error: Option<String>,
  message: String
}

mod hooks;
use hooks::HookEvent;


fn handle_webhook(request: &mut Request) -> IronResult<Response> {
  let content_type = "application/json".parse::<Mime>().unwrap();
  let data = request.get::<bodyparser::Json>();

  match data {
    Ok(Some(data)) => {
      let response = JsonResponse { error: None, message: "Receive request with success!".to_string() };
      let serialized = serde_json::to_string(&response).unwrap();

      let event = request.headers.get_raw("x-gitlab-event").unwrap();
      let event = HookEvent::from(event, data).unwrap();

      hooks::handler::handle(event);

      Ok(Response::with((content_type, status::Ok, serialized)))
    },
    _ => {
      let response = JsonResponse { error: Some("Bad Request".to_string()), message: "Body is missing.".to_string() };
      let serialized = serde_json::to_string(&response).unwrap();

      Ok(Response::with((content_type, status::BadRequest, serialized)))
    }
  }
}

fn main () {
  let router = router!(webhook: post "/" => handle_webhook);

  let _server = Iron::new(router).http("localhost:8601")
    .expect("Can't open Iron server.");

  println!("Listening on :8601");
}
