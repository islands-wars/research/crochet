/*
 * Copyright (C) 2018 Islands Wars
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 **/

use serde_json::Value;
use super::HookEvent;
use serenity::builder::CreateEmbed;
use serenity::utils::Colour;
use md5;
use rand::{self, Rng};
use reqwest;
use std::env;

/// Macro for transform icon name into an octicon or an email into a Gravatar picture.
macro_rules! icon {
  ($icon:tt) => (&format!("https://github.com/webdog/octicons-png/blob/master/{}.svg.png?raw=true", $icon));
  ($email:expr) => (&format!("https://gravatar.com/avatar/{:x}", md5::compute(unpack!(str: $email).trim().to_lowercase())))
}

/// Unwrap `serde_json` values and set a default value (when unwrap fail).
macro_rules! unpack {
  (array: $e:expr) => ($e.as_array().unwrap_or(&Vec::new()));
  (length: $e:expr) => ($e.as_array().unwrap_or(&Vec::new()).len());
  (str: $e:expr) => ($e.as_str().unwrap_or(""));
}

/// Choose a random value between all the proposed one.
macro_rules! choose {
  ($($image:tt)*) => (rand::thread_rng().choose(&vec!($($image)*)).unwrap_or(&""))
}

/// Execute a webhook, take embeds and send them to Discord.
fn execute_webhook (embeds: Vec<Value>) {
  match env::var("DISCORD_WEBHOOK_URL") {
    Ok(value) => {
      let content = json!({
        "embeds": embeds
      });

      let client = reqwest::Client::new();
      let _response = client.post(&value)
        .json(&content)
        .send().unwrap();
    },
    Err(_) => println!("Couldn't send the webhook to Discord because 'DISCORD_WEBHOOK_URL' isn't set."),
  }
}

/// Handle a `HookEvent` event.
pub fn handle (event: HookEvent) {
  match event {
    HookEvent::Push(data) => {
      println!("{:?}", data);

      let branch = unpack!(str: data["ref"]);
      let branch = branch.split('/').last().unwrap();

      let mut embeds: Vec<Value> = Vec::new();

      for commit in (unpack!(array: &data["commits"])).iter() {
        let embed = create_embed(|embed| embed
          // Set embed's title from Project -> Path with namespace
          .title(unpack!(str: data["project"]["path_with_namespace"]))
          // Set embed's description from branch, project's homepage and commit's message
          .description(format!("Committed on [{branch}]({homepage}/tree/{branch}).\n{message}",
            branch = branch,
            homepage = unpack!(str: data["project"]["homepage"]),
            message = unpack!(str: commit["message"])))
          // Set a blue color
          .color(Colour::new(2201331))
          // Set embed's url from commit's url
          .url(unpack!(str: commit["url"]))
          // Set embed's timestamp from commit's timestamp
          .timestamp(unpack!(str: commit["timestamp"]))
          .author(|author| author
            .name(unpack!(str: commit["author"]["name"]))
            .icon_url(icon!(commit["author"]["email"])))
          .field(|field| field
            .name("📜 File informations")
            .value(format!("There are {} files added, {} modified and {} deleted",
              unpack!(length: commit["added"]),
              unpack!(length: commit["modified"]),
              unpack!(length: commit["removed"])))
            .inline(false))
          .footer(|footer| footer
            .icon_url(icon!("repo-push"))
            .text("Push Event!"))
          .thumbnail(unpack!(str: data["project"]["avatar_url"]))
          .image(choose!(
            "https://media.giphy.com/media/AbDb2PniluFwY/giphy.gif",
            "https://media1.tenor.com/images/4a1a5e2c2bb61fdaeaf2e0b912fe9fe5/tenor.gif",
            "https://media1.tenor.com/images/4c147c241198aafdeedde65d4b78d0f6/tenor.gif"
          )));
        embeds.push(embed);
      }
      println!("Push event triggered, receive {:?} commits.", embeds.len());
      execute_webhook(embeds)
    },
    _ => unimplemented!()
  }
}

/// Create a new Embed.
/// Take from zeyla/serenity/src/model/channel/embed.rs#L90-L93
fn create_embed<F>(f: F) -> Value
where F: FnOnce(CreateEmbed) -> CreateEmbed {
  Value::Object(f(CreateEmbed::default()).0)
}
