/*
 * Copyright (C) 2018 Islands Wars
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 **/

use serde_json::Value;
use std::str;

/// The `HookEvent` enum.
pub enum HookEvent {
  /// Triggered when you push to the repository except when pushing tags.
  Push(Value),
  /// Triggered when you create (or delete) tags to the repository.
  TagPush(Value),
  /// Triggered when a new issue is created or an existing issue was updated/closed/reopened.
  Issue(Value),
  /// Triggered when a new comment is made on commits, merge requests, issues, and code snippets.
  Note(Value),
  /// Triggered when a new merge request is created, an existing merge request was
  /// updated/merged/closed or a commit is added in the source branch.
  MergeRequest(Value),
  /// Triggered when a wiki page is created, updated or deleted.
  WikiPage(Value),
  /// Triggered on status change of Pipeline or when Xharos want to see awesome Gifs.
  Pipeline(Value),
  /// Triggered on status change of Build.
  Build(Value)
}

impl HookEvent {
  pub fn from(event: &[Vec<u8>], data: Value) -> Option<HookEvent> {
    match str::from_utf8(&event[0]) {
      Ok(event) => match event {
        "Push Hook" => Some(HookEvent::Push(data)),
        "Tag Push Hook" => Some(HookEvent::TagPush(data)),
        "Issue Hook" => Some(HookEvent::Issue(data)),
        "Note Hook" => Some(HookEvent::Note(data)),
        "Merge Request Hook" => Some(HookEvent::MergeRequest(data)),
        "Wiki Page Hook" => Some(HookEvent::WikiPage(data)),
        "Pipeline Hook" => Some(HookEvent::Pipeline(data)),
        "Build Hook" => Some(HookEvent::Build(data)),
        _ => None
      }
      Err(_) => None
    }
  }
}
